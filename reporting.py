#!/usr/bin/python3.8

""" Performance plots """

from math import exp, pi, sqrt, factorial
import collections
import logging
import matplotlib  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import scipy.optimize
# from scipy.stats import norm  # type: ignore

matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
    'axes.labelsize': 8,
    'ytick.labelsize': 8,
    'xtick.labelsize': 8,
    'font.serif': 'Times'
})


def format_report(report):
    """ Formats the dict report into a printable string """
    report_str = ""
    for key, value in sorted(report.items(), key=lambda x: x[0]):
        logging.log(25, "%d trajectories covered at %d percent.",
                    len(value), key)

    # lengths = collections.defaultdict(int)
    # total = 0
    # for traj in report[0]:
    #     lengths[len(traj)] += 1
    # for key, value in sorted(lengths.items(), key=lambda x: x[0]):
    #     logging.log(25, "%d uncovered trajectories of length %d",
    #                 value, key)
    #     total += value
    # print(total)
    return report_str


def report_crossed_nodes(report):
    """ Reports how many nodes cross "value" amount of trajectories. """
    report2 = collections.defaultdict(int)
    for value in report.values():
        report2[value] += 1
    return report2


def format_vc(report):
    """ Formats and prints the report. """
    report_str = ""
    for key, value in sorted(report.items(), key=lambda x: x[0]):
        logging.log(25, "%d nodes covering %d trajectories", value, key)
    return report_str


def report_seen_percent(graph, report, j):
    """ Reports the percentage of trajectories that have ]j-1,j]% of their
    nodes covered """
    amount_traj = len(report[j])
    # logging.log(30, "Number of trajectories covered ]%d,%d] percent:  %d"
    #             " %f%%", j-1, j, amount_traj,
    #             amount_traj / len(graph.trajectories)*100)
    return amount_traj / len(graph.trajectories) * 100


def report_seen_less_percent(graph, report, j):
    """ Reports the CDF percentage of trajectories that have less of j% of
    their nodes covered """
    amount_traj = sum([len(x) for x in [report[i] for i in range(j+1)]])
    # logging.log(30, "Number of trajectories covered less than %d percent"
    #                 ":  %d %f%%", j,
    #                 amount_traj, amount_traj / len(graph.trajectories)*100)
    return amount_traj / len(graph.trajectories) * 100


def report_all_percents_cdf(graph, report, pad):
    """ Reports the percentage of trajectories that have less of j% of their
    nodes covered for some values of j """
    res = []
    for i in range(0, 100+1, pad):
        res.append(report_seen_less_percent(graph, report, i))
    return res


def report_all_percents_nc(graph, report, pad):
    """ Reports the percentage of trajectories that have less of j% of their
    nodes covered for some values of j """
    res = []
    for i in range(0, 100+1, pad):
        res.append(report_seen_percent(graph, report, i))
    return res


def lost_by_length_nc(lost_threshold, report):
    """ Returns a dictionnary whose keys are lost trajectories length and whose
    values are the ratio between the lost trajectories of key length among all
    trajectories of key length """
    lost_length = collections.defaultdict(int)
    caught_length = collections.defaultdict(int)
    for percentage, trajectories in report.items():
        for trajectory in trajectories:
            if percentage > lost_threshold:
                caught_length[len(trajectory)] += 1
                # In case a certain length has been caught 100%
                if len(trajectory) not in lost_length:
                    lost_length[len(trajectory)] = 0
            else:
                lost_length[len(trajectory)] += 1
    for length, amount_traj in lost_length.items():
        lost_length[length] /= amount_traj + caught_length[length]
        lost_length[length] *= 100
    return lost_length


def lost_by_length_cdf(lost_threshold, graph, report):
    """ Returns a dictionnary whose keys are the ordered length of lost
    trajectories and whose values are the cumulative percentage of trajectories
    (percentage of lost trajectories of length <= l, across all trajectories.
    """
    lost_length = collections.defaultdict(int)
    for percentage, trajectories in report.items():
        for trajectory in trajectories:
            if percentage <= lost_threshold:
                lost_length[len(trajectory)] += 1
    for length in lost_length:
        lost_length[length] /= len(graph.trajectories)
        lost_length[length] *= 100
    lost_length_percent_sorted = [x[1] for x in sorted(lost_length.items())]
    res = dict()
    if lost_length_percent_sorted:
        cdf_lost_length = [lost_length_percent_sorted[0]]
        for i in lost_length_percent_sorted[1:]:
            cdf_lost_length.append(cdf_lost_length[-1]+i)
        cdf_length = [x[0] for x in sorted(lost_length.items())]
        for i, j in zip(cdf_length, cdf_lost_length):
            res[i] = j
    return res


def high_lost_nodes_nc(lost_threshold, graph, report):
    """ This function creates a dictionnary whose keys are node IDs and whose
    values are the number (percentage) of lost trajectories that go through
    that node. """
    def __amount_per_node__(trajectories, amount_per_node):
        for trajectory in trajectories:
            already_seen = set()
            for node1, node2 in trajectory:
                if node1 not in already_seen:
                    amount_per_node[node1] += 1
                if node2 not in already_seen:
                    amount_per_node[node2] += 1
                already_seen.add(node1)
                already_seen.add(node2)
    # This is the amount of lost trajectories that go through 'key' node. A
    # lost trajectory will be counted as many times as its length
    amount_lost_per_node = collections.defaultdict(int)
    # This is the number of trajectories that go through 'key' node. A
    # trajectory will be counted as many times as its length
    amount_traj_per_node = collections.defaultdict(int)
    for percentage, trajectories in report.items():
        if percentage <= lost_threshold:
            __amount_per_node__(trajectories, amount_lost_per_node)
        __amount_per_node__(trajectories, amount_traj_per_node)
    # diviser amount lost per node par le nombre total de trajectoires ?
    # Turns into percents
    total = 0
    for traj in graph.trajectories:
        traj_nodes = set()
        for node1, node2 in traj:
            traj_nodes.add(node1)
            traj_nodes.add(node2)
        total += len(traj_nodes)
    for node in amount_lost_per_node:
        # amount_lost_per_node[node] /= total
        amount_lost_per_node[node] /= len(graph.trajectories)
        amount_lost_per_node[node] *= 100
    return amount_lost_per_node


def high_lost_nodes_cdf(amount_lost_per_node):
    """ percent_lost_per_node is a dictionnary of cumulative percentages of
    lost trajectories that go through each node, ordered by percentages."""
    # Gets the percentage amount of lost trajectories that go through each
    # node. Sorted by node ID.
    percent_lost_per_node = dict()
    if amount_lost_per_node:
        lost_traj_amount = [x[1] for x in list(reversed(sorted(
            amount_lost_per_node.items(), key=lambda x: x[1])))]
        lost_traj_amount_cdf = [lost_traj_amount[0]]
        for i in lost_traj_amount[1:]:
            lost_traj_amount_cdf.append(lost_traj_amount_cdf[-1]+i)
        # This data generates percent_lost_per_node.pgf
        # Gets the sorted nodes id
        nodes_id = [x[0] for x in list(reversed(sorted(
            amount_lost_per_node.items(), key=lambda x: x[1])))]
        for node, percent in zip(nodes_id, lost_traj_amount_cdf):
            percent_lost_per_node[node] = percent
    return percent_lost_per_node


def lengths_distrib(graph):
    """ Creates a dictionnary containing the number of trajectories of each
    length """
    lengths = collections.defaultdict(int)
    for traj in graph.trajectories:
        lengths[len(traj)] += 1
    return lengths


def draw_lengths_distrib(graph, filename):
    """ Draws the lengths distributions of trajectories (amount of traj. for
    each length in number of edges) """
    lengths = lengths_distrib(graph)
    plt.figure(figsize=(3.4, 2))
    plt.bar(lengths.keys(), lengths.values())
    plt.xlabel('Number of street sections')
    plt.ylabel('Amount of trajectories')
    plt.tight_layout()
    plt.savefig(filename)
    plt.close()


def draw_lost_length_mean(length, percent_lost, filename):
    """ Draws the amount of trajectories as a function of how much they are
    seen. """

    plt.figure(figsize=(4.2, 2.3))
    plt.bar(length, percent_lost, label='lost')
    plt.ylabel('Percent of trajectories lost')
    plt.xlabel('Trajectory length in number of edges')
    plt.tight_layout()
    plt.savefig(filename)
    plt.close()


def draw_lost_length_cdf(percent_lost, filename):
    """ Draws the cumulative distribution function of "the amount of
    trajectories as a function of how much they are seen". """

    plt.figure(figsize=(4.2, 2.3))
    plt.plot(percent_lost.keys(), percent_lost.values(), label='lost')
    plt.ylabel('Percent of trajectories lost')
    plt.xlabel('Trajectory length in number of edges')
    plt.tight_layout()
    plt.savefig(filename)
    plt.close()


def draw_percent_lost_nc(percent_lost, filename):
    """ Draws the amount of lost trajectories as a function of the lengths """

    def gaussian(xs, amplitude, mean, stddev):
        """ Gaussian curve """
        return [amplitude * exp(-((x - mean) / 4 / stddev)**2) for x in xs]


    def normal(xs, mu, sigma):
        """ Normal distribution """
        return [(1/(sigma * sqrt(2*pi))) * exp(-1/2 * (((x-mu)/sigma)**2))
                for x in xs]


    def poisson(ks, λ):
        """ Poisson distributoin function """
        return [(exp(-λ) * λ ** k) / factorial(k) for k in ks]

    data = percent_lost
    x = range(len(data))

    # max_pl = max(data)
    # data_normalized = [p/max_pl for p in data]

    max_x = max(x)
    x_normalized = [r/max_x for r in x]

    plt.figure(figsize=(4.2, 2.3))
    plt.bar(x_normalized, data, width=0.0075)
    # plt.bar(x, data)
    # plt.plot(x, data)

    # popt, _ = scipy.optimize.curve_fit(gaussian, x_normalized, data)
    # plt.plot(x_normalized, gaussian(x_normalized, *popt), label="gaussian")
    # print("gaussian", popt)

    popt, _ = scipy.optimize.curve_fit(normal, x_normalized, data)
    plt.plot(x_normalized, normal(x_normalized, *popt),
             label="normal, $\mu$={}, $\sigma$={}".format(
                "{:.2f}".format(popt[0]),
                "{:.2f}".format(popt[1])),
             color="tab:orange")
    # print("normal", popt)

    # popt, _ = scipy.optimize.curve_fit(poisson, x, data)
    # plt.plot(x, poisson(x, *popt), label="poisson")
    # print("poisson", popt)

    plt.xlabel('Percent of nodes seen per trajectory')
    plt.ylabel('Percent of trajectories')

    plt.legend(loc="upper right", fontsize="x-small", ncol=4)
    plt.tight_layout()
    plt.savefig(filename)
    plt.close()


def draw_percent_lost_per_node(data, filename):
    """ Draws the cumulative distribution function of "the amount of lost
    trajectories as a function of the lengths" """

    percent_lost_nc = [x[1] for x in
                       list(reversed(sorted(data.items(),
                                            key=lambda x: x[1])))]
    nodes = list(range(len(data)))
    nodes = [n+1 for n in nodes]

    plt.figure(figsize=(4.2, 2.3))

    plt.plot(nodes, percent_lost_nc, label='lost')
    plt.xlabel('Node')
    plt.ylabel('% of trajectories lost')
    plt.xscale("log")
    plt.tight_layout()
    plt.savefig(filename)
    plt.close()


def draw_percent_lost(percent_lost, filename):
    """ For each node, the amount of lost trajectories that go through it """

    plt.figure(figsize=(4.2, 2.3))

    plt.plot(range(101), percent_lost, label='lost')
    plt.xlabel('Percent of nodes seen per trajectory')
    plt.ylabel('Cumulative percent of trajectories')

    # mu, std = norm.fit(percent_lost)
    # print("mu", mu, "std", std)
    # p = norm.cdf(range(101), mu, std)
    # plt.plot(range(101), p, 'k', linewidth=2)

    plt.tight_layout()
    plt.savefig(filename)
    plt.close()

#!/usr/bin/python3

""" main

    Dependencies on Ubuntu 21.04: sudo apt install python3 python-is-python3
    python3-cartopy python3-networkx python3-overpy python3-matplotlib
    python3-scipy

"""

import logging
import argparse
import time
import collections
import sys
import random
import reporting
from k_vertex_cover import Graph
from heatmap import heat

# If considering the old definition of Lost (fixed number of nodes), then LOST
# is the number of nodes that need to be seen to consider a trajectory caught.
# If considering the new definition of Lost (percentage of nodes), then LOST is
# the percentage of nodes that need to be seen to consider a trajectory caught.
# LOST_THRESHOLD = 20
# OLD_DEF_LOST = False


def positive_int(value):
    """ Checks that value is a positive integer """
    value_int = int(value)
    if value_int < 0:
        raise argparse.ArgumentTypeError("%s needs to be a positive integer"
                                         % value)
    return value_int


def naive_placement(graph, number_nodes, args, notraj=False):
    """ Naive placement algorithm. Data structure isn't the best but this isn't
    made to be actually used
    This is a greedy algorithm that picks the node through which the largest
    amount of lost trajectories go """
    # Dirty hack
    if notraj:
        number_nodes = G.graph.number_of_nodes()
    graph.vertex_cover = set()
    graph.k_vertex_cover = set()
    while len(graph.vertex_cover) < number_nodes:
        # ajouter le sommet qui impact le plus la perte ?
        hln = reporting.high_lost_nodes_nc(
            args.lost_threshold, graph,
            graph.report_trajectories(args.old_def_lost, args.l, args.k,
                                      args.coords))
        # In case there is no lost trajectories, stop adding sniffers
        if not hln:
            graph.k_vertex_cover = graph.vertex_cover
            return
        # We might chose a node that is already chosen since adding the
        # node through which the most lost trajs go might not remove many
        # trajs from the losts.
        node_to_add = max(hln, key=hln.get)
        while node_to_add in graph.vertex_cover:
            del hln[node_to_add]
            if not hln:
                graph.k_vertex_cover = graph.vertex_cover
                return
            node_to_add = max(hln, key=hln.get)
        logging.log(20, "%d out of %d adding %d", len(graph.vertex_cover),
                    number_nodes, node_to_add)
        graph.vertex_cover.add(node_to_add)

        # Dirty hack
        if notraj:
            graph.k_vertex_cover = graph.vertex_cover
            report = G.report_trajectories(ARGS.old_def_lost, ARGS.l, ARGS.k, ARGS.coords)
            lost = sum([len(x) for x in [report[i] for i in range(ARGS.lost_threshold+1)]])/len(G.trajectories)*100
            print("Percent lost", lost, type(lost))
            if not lost:
                number_nodes = 0

    graph.k_vertex_cover = graph.vertex_cover


def naive_placement2(graph, number_nodes, notraj=False):
    """ Naive placement algorithm. Data structure isn't the best but this isn't
    made to be actually used.
    This is a greedy algorithm that picks the node through which the largest
    amount of trajectories go.
    The maximum number of nodes it will pick is the number of nodes that have
    at least one traj going through it"""
    # Dirty hack
    if notraj:
        number_nodes = G.graph.number_of_nodes()
    graph.vertex_cover = set()
    graph.k_vertex_cover = set()
    number_traj_by_node = collections.defaultdict(int)
    for trajectory in graph.trajectories:
        for edge in trajectory:
            number_traj_by_node[edge[0]] += 1
        number_traj_by_node[trajectory[-1][-1]] += 1
    # ajouter le sommet par lequel le plus de trajectoires passe ?
    node_to_add = max(number_traj_by_node, key=number_traj_by_node.get)
    while len(graph.vertex_cover) < number_nodes:
        while node_to_add in graph.vertex_cover:
            del number_traj_by_node[node_to_add]
            if number_traj_by_node:
                node_to_add = max(number_traj_by_node,
                                  key=number_traj_by_node.get)
            else:
                number_nodes = len(graph.vertex_cover)
                break
        graph.vertex_cover.add(node_to_add)

        # Dirty hack
        if notraj:
            graph.k_vertex_cover = graph.vertex_cover
            report = G.report_trajectories(ARGS.old_def_lost, ARGS.l, ARGS.k, ARGS.coords)
            lost = sum([len(x) for x in [report[i] for i in range(ARGS.lost_threshold+1)]])/len(G.trajectories)*100
            print("Percent lost", lost, type(lost))
            if not lost:
                number_nodes = 0

    graph.k_vertex_cover = graph.vertex_cover


if __name__ == "__main__":
    # Set parser
    PARSER = argparse.ArgumentParser(prog="main.py",
                                     description="""Computes a 2-approximation
                                     of the vertex cover then uses centrality
                                     to determine which nodes are important
                                     waypoints where sensors should be
                                     placed""",
                                     # epilog=""
                                     )
    PARSER.add_argument("graph_file", help="Path to a graph file formatted "
                        "like \"node space" " node\"", type=str)
    PARSER.add_argument("-t", "--trajectories", help="Path to a pickle "
                        "trajectories file containing a list of lists of "
                        "tuples of two ints", type=str)
    PARSER.add_argument("-d", "--debug", help="Will check the solution is "
                        "correct once finished", action="store_true")
    PARSER.add_argument("-v", "--verbose", help="Turn on verbosity",
                        action="store_true")
    PARSER.add_argument("--length", help="Length of the shortest paths for the"
                                         " accessibility centrality",
                        default=4, type=positive_int)
    PARSER.add_argument("-k", help="Minimal distance between sensors",
                        default=0, type=positive_int)
    PARSER.add_argument("-l", help="Area of effect of sensors in metres",
                        default=25, type=positive_int)
    PARSER.add_argument("--greedy", help="Greedy algorithm", default="",
                        choices=["lost", "traj", "random"],
                        type=str)
    PARSER.add_argument("-c", "--centrality", help="Centrality type. It can be"
                        " one of: katz, information, degree, eigenvector, "
                        "closeness, betweenness accessibility"
                        " expected_force, weighted_deg or combination",
                        default="",
                        choices=["katz", "degree", "information",
                                 "eigenvector", "closeness", "betweenness",
                                 "accessibility", "expected_force",
                                 "expected_force_nolog", "weighted_deg",
                                 "combination"],
                        type=str)
    PARSER.add_argument("-p", "--plot", help="File where the map will be "
                        "plotted. If no extension is given, defaults to png."
                        " Supported formats include png, jpg, pdf, svf, pgf.",
                        type=str)
    PARSER.add_argument("--heatmap", help="Name of the file where the heatmap"
                        " will be stored", type=str)
    PARSER.add_argument("--start", help="Beginning of the time window to"
                        " consider for the heatmap.", default=0,
                        type=positive_int)
    PARSER.add_argument("--end", help="End of the time window to consider for"
                        " the heatmap", default=24*3600-1, type=positive_int)
    PARSER.add_argument("--coords", help="File containing the geo-coordinates "
                        "of all nodes of the network", type=str)
    PARSER.add_argument("--time", help="File containing a dictionnary matching"
                        " the nodes with a list of timestamps at which they "
                        "are visited", type=str)
    # PARSER.add_argument("--kauto", help="Find the k that minimizes the"
    #                                     " efficiency", action="store_true")
    PARSER.add_argument("--curves", help="Draw curves to analyze the results",
                        action="store_true")
    PARSER.add_argument("--lost_threshold", help="Threshold below which"
                        " trajectories are considered lost",
                        default=20, type=positive_int)
    PARSER.add_argument("--until_no_lost", help="Meant to be used with the"
                        " greedy approaches. Makes them run until no traj is"
                        " lost", action="store_true")
    PARSER.add_argument("--old_def_lost", help="Triggers the old definition of"
                        " lost, which an absolute number of time a trajectory"
                        " has to be seen to be considered reconstructible."
                        " When turned off, the new definition applies, with"
                        " which a trajectory has to be seen a certain"
                        " percentage of time to be considered reconstructible."
                        " That is, if a trajectory has x percent of its"
                        " vertices seen, it is considered reconstructible. x"
                        " being set with --lost_threshold. Trajectories with a"
                        " length lower than what is set here will be"
                        " discarded",
                        action="store_true")
    ARGS = PARSER.parse_args()

    # Set logger
    if ARGS.verbose:
        logging.basicConfig(format='%(message)s', level=10)
    else:
        logging.basicConfig(format='%(message)s', level=25)

    if ARGS.centrality == "weighted_deg" and not ARGS.trajectories:
        logging.log(35, "Cannot weigh the graph if no trajectories are given")
        sys.exit()

    # Import graph
    START1 = time.process_time()
    G = Graph(ARGS.graph_file)
    END1 = time.process_time()
    logging.log(25, "Import time %f seconds", END1 - START1)

    # Processing
    START2 = time.process_time()

    # Importing trajectories
    if ARGS.trajectories:
        logging.log(25, "Importing trajectories…")
        # This is to generate the shortest path between all pairs of nodes
        # G.trajectories = G.__generate_all_shortest_paths__()
        # ARGS.trajectories = True
        # This is to use the trajectory file provided
        G.import_trajectories(ARGS.trajectories)
        if ARGS.old_def_lost:
            traj_thresholded = []
            for traj in G.trajectories:
                if len(traj) > ARGS.lost_threshold:
                    traj_thresholded.append(traj)
            G.trajectories = traj_thresholded
        if ARGS.centrality in ["weighted_deg"]:
            # Weights the edges depending on the trajectories
            G.__add_weight__()

    # If asked to find the best k, start with k=10
    # if ARGS.kauto and not ARGS.k:
    #     ARGS.k = 10
    # For naive tests:
    if ARGS.greedy == "lost":
        if ARGS.coords and ARGS.trajectories:
            naive_placement(G, ARGS.k, ARGS)
        if ARGS.until_no_lost:
            naive_placement(G, ARGS.k, ARGS, notraj=True)
    elif ARGS.greedy == "traj":
        if ARGS.trajectories:
            naive_placement2(G, ARGS.k)
        if ARGS.until_no_lost:
            naive_placement2(G, ARGS.k, notraj=True)
    elif ARGS.greedy == "random":
        if ARGS.k > len(G.graph.nodes()):
            ARGS.k = len(G.graph.nodes())
        G.vertex_cover = set(random.sample(list(G.graph.nodes()), k=ARGS.k))
        G.k_vertex_cover = G.vertex_cover
    # For random placement: (currently not working)
    # ARGS.k = 1
    # G.centrality_sample(ARGS.k, ARGS.centrality, 200)
    # Actual method:
    else:
        G.k_vertex_cover_wrapper(ARGS)
    # Checking correctness.
    if ARGS.debug:
        logging.log(25, "Checking correctness…")
        G.correctness(ARGS.k)
    # G.naive_approx()
    END2 = time.process_time()
    logging.log(25, "Processing time %f seconds", END2 - START2)

    # Checking trajectories
    if ARGS.trajectories:
        logging.log(25, "Checking trajectories…")
        # REPORT = G.report_trajectories_old(ARGS.l, ARGS.k)
        if ARGS.coords:
            REPORT = G.report_trajectories(ARGS.old_def_lost, ARGS.l, ARGS.k,
                                           ARGS.coords)
        else:
            logging.log(30, "A coordinate file must be provided to compute "
                        "the nodes within reach of sniffers")
            sys.exit()
        # format_report(REPORT)
        # format_vc(report_crossed_nodes(G.report_vc(REPORT)))

    # Reporting
    logging.log(30, "Total time:    %f seconds", END2 - START1)
    logging.log(30, "Number of nodes:   %d", len(G.graph.nodes()))
    logging.log(30, "Number of edges:   %d", len(G.graph.edges()))
    logging.log(30, "Size of the vertex cover:  %d %f%%", len(G.vertex_cover),
                len(G.vertex_cover)/len(G.graph.nodes())*100)
    # NP-hard <1.3606, our bound: 5/3
    # logging.log(30, "Average degree is %f, making it a %f-approximation",
    #             G.average_degree, 2-1/(G.average_degree))
    if ARGS.k:
        logging.log(30, "Size of the k vertex cover:    %d %f%%",
                    len(G.k_vertex_cover),
                    len(G.k_vertex_cover)/len(G.graph.nodes())*100)
        logging.log(30, "Using centralty:  %s", ARGS.centrality)
    if ARGS.coords:
        logging.log(30, "Size of the area: %f km²", G.size_area(ARGS.coords))
    if ARGS.trajectories:
        logging.log(30, "Number of trajectories:    %d", len(G.trajectories))
        # print("Number of trajectories covered less than index percent:",
        #       report_all_percents_cdf(REPORT, 1))
        logging.log(30, "Efficiency2: %f",
                    (1-(len(G.k_vertex_cover)/len(G.graph))) *
                    (1-(sum([len(x) for x in
                            [REPORT[i] for i in range(ARGS.lost_threshold+1)]])
                     / len(G.trajectories))))
        logging.log(30, "Efficiency: %f",
                    (len(G.k_vertex_cover)/len(G.graph)*100) +
                    sum([len(x) for x in
                         [REPORT[i] for i in range(ARGS.lost_threshold+1)]])
                    / len(G.trajectories)*100)
        logging.log(40, "k=%d, Efficiency: %f + %f = %f, eff2=%f",
                    ARGS.k,
                    (len(G.k_vertex_cover)/len(G.graph)*100),
                    sum([len(x) for x in
                         [REPORT[i] for i in range(ARGS.lost_threshold+1)]])
                    / len(G.trajectories)*100,
                    (len(G.k_vertex_cover)/len(G.graph)*100) +
                    sum([len(x) for x in
                        [REPORT[i] for i in range(ARGS.lost_threshold+1)]]) /
                    len(G.trajectories)*100,
                    (1-(len(G.k_vertex_cover)/len(G.graph))) *
                    (1-(sum([len(x) for x in
                            [REPORT[i] for i in range(ARGS.lost_threshold+1)]])
                     / len(G.trajectories))))
        # if ARGS.kauto:
        #     k = dict()
        #     k[ARGS.k] = ((len(G.k_vertex_cover)/len(G.graph)*100),
        #                  sum([len(x) for x in
        #                       [REPORT[i] for i in range(LOST_THRESHOLD+1)]])
        #                  / len(G.trajectories)*100)
        #     k[ARGS.k] = (k[ARGS.k][0], k[ARGS.k][1],
        #                  k[ARGS.k][0] + k[ARGS.k][1])
        #     print(k)
        # goto? And chose a new k
        # file_object = open('perf_cologne_downtown_expected_force.txt', 'a')
        # file_object.write(
        #     '{} {} {} {}\n'.format(
        #         ARGS.k,
        #         (len(G.k_vertex_cover)/len(G.graph)*100),
        #          sum([len(x) for x in [REPORT[i] for i in range(10+1)]])
        #          / len(G.trajectories)*100)
        #         ARGS.centrality
        #     ))
        # file_object.close()
        # print("-------------------------------------------------------------")
        # percent_lost_nc.pgf
    if ARGS.curves:
        if not ARGS.trajectories or not ARGS.coords:
            logging.log(35, "Trajectories and coordinates must be provided"
                            " to draw curves.")
            sys.exit()
        else:
            if ARGS.old_def_lost:
                DEF_LOST = "old_def"
            else:
                DEF_LOST = "new_def"
            logging.log(25, "Generating curves…")
            reporting.draw_percent_lost_nc(
                reporting.report_all_percents_nc(G, REPORT, 1),
                "{}_{}_{}_{}_{}_percent_lost_nc.pgf".format(
                    ARGS.graph_file.split('.')[-2].split('/')[-1], DEF_LOST,
                    ARGS.lost_threshold, ARGS.centrality, ARGS.k),
                    ARGS.old_def_lost)
            reporting.draw_percent_lost(
                reporting.report_all_percents_cdf(G, REPORT, 1),
                "{}_{}_{}_{}_{}_percent_lost.pgf".format(
                    ARGS.graph_file.split('.')[-2].split('/')[-1], DEF_LOST,
                    ARGS.lost_threshold, ARGS.centrality, ARGS.k))
            reporting.draw_lost_length_mean(
                [z[0] for z in sorted(
                    reporting.lost_by_length_nc(
                        ARGS.lost_threshold, REPORT).items())],
                [z[1] for z in sorted(
                    reporting.lost_by_length_nc(
                        ARGS.lost_threshold, REPORT).items())],
                "{}_{}_{}_{}_{}_lost_by_length_nc.pgf".format(
                    ARGS.graph_file.split('.')[-2].split('/')[-1], DEF_LOST,
                    ARGS.lost_threshold, ARGS.centrality, ARGS.k))
            reporting.draw_lost_length_cdf(
                reporting.lost_by_length_cdf(ARGS.lost_threshold, G, REPORT),
                "{}_{}_{}_{}_{}_lost_length_cdf.pgf".format(
                    ARGS.graph_file.split('.')[-2].split('/')[-1], DEF_LOST,
                    ARGS.lost_threshold, ARGS.centrality, ARGS.k))
            reporting.draw_percent_lost_per_node(
                reporting.high_lost_nodes_nc(ARGS.lost_threshold, G, REPORT),
                "{}_{}_{}_{}_{}_percent_lost_per_node_nc.pgf".format(
                    ARGS.graph_file.split('.')[-2].split('/')[-1], DEF_LOST,
                    ARGS.lost_threshold, ARGS.centrality, ARGS.k))
            reporting.draw_lengths_distrib(G, "lengths_{}.pgf".format(
                    ARGS.graph_file.split('.')[-2].split('/')[-1]))
            logging.log(25, "Curves done")

    # Plotting
    if ARGS.plot:
        logging.log(25, "Plotting cover…")
        if ARGS.coords:
            if ARGS.trajectories:
                G.plot_cover(ARGS, REPORT[0])
            else:
                G.plot_cover(ARGS)
            logging.log(25, "Plot exported to %s", ARGS.plot)
        else:
            logging.log(30, "A coordinate file must be provided to plot on a "
                        "map")

    if ARGS.heatmap:
        # To plot a heatmap of all trajectories
        # if ARGS.time:
        #     nheat = heat(ARGS.time, ARGS.start, ARGS.end)
        # else:
        #     logging.log(30, "A timestamp file must be provided to generate a"
        #                 " heatmap")
        #     sys.exit()
        # To plot a heatmap of lost nodes:
        # nheat = high_lost_nodes_nc(REPORT)
        # For 250m squares: 137
        # For whole map: 1000
        # For downtown Cologne: 100
        if ARGS.coords:
            # G.plot_heatmap_ugly(nheat, ARGS.heatmap, 100, ARGS.coords)
            G.plot_heatmap(ARGS.heatmap, ARGS.coords)
        else:
            logging.log(30, "A coordinate file must be provided to plot a "
                        "heatmap")

    # Reporting memory consumption (time consuming)
    # if args.verbose:
    #     logging.log(15, str(guppy.hpy().heap()))

#!/usr/bin/python3

""" Handles the generation of nodes' heat """

import pickle
import collections
import logging


def heat(nodes_path, start, end):
    """ Assigns to each node, the number of times it is crossed by a trajectory
    """
    nodes_file = open(nodes_path, 'rb')
    logging.log(25, "Loading timestamps in memory…")
    nodes_timestamped = pickle.load(nodes_file)
    nodes_file.close()
    nodes_heat = collections.defaultdict(int)
    max_heat = 0
    logging.log(25, "Generating heat…")
    count = 0
    for node, timestamps in nodes_timestamped.items():
        count += 1
        nodes_heat[node] = len([t for t in timestamps if start <= t <= end])
        max_heat = max(max_heat, nodes_heat[node])
        if count % 1000 == 0:
            logging.log(25, "%d", count)
    # To get heat in [0,1]
    # for node in nodes_heat:
    #     nodes_heat[node] /= max_heat
    return nodes_heat

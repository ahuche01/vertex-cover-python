#!/usr/bin/python3

""" Infers a linear combination of centrality. Idea from
https://doi.org/10.1093/comnet/cnz048, section 3.3

See https://doi.org/10.1016/j.socnet.2010.03.006 for more weighted centralities
(degree, betweenness and closeness)
"""

import scipy.stats


def combination(cent_vector):
    """ Computes a linear combination of centralities """

    taus = list()
    for i in range(1, 9):
        taus.append(scipy.stats.kendalltau([x[9] for x in cent_vector],
                                           [x[i] for x in cent_vector])[0])

    # print("taus = ", taus)
    # print("katz degree information betweenness closeness accessibility"
    #       " eigenvector expected_force")

    return [tau/sum(taus) for tau in taus]

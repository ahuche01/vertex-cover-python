This is an implementation of a sniffer placement algorithm, for trajectory
reconstruction and contact tracing.

The algorithm uses [Bar-Yehuda and Even's 5/3-approximation
algorithm](https://doi.org/10.1145/800070.802205) for the vertex cover problem
on planar graphs, along with a heuristic.

## Dependencies

`networkx` for graph operations, `overpy` to retrieve OSM data, `scipt` for
curve fitting, `matplotlib` and `cartopy` to plot the results.

## Usage

```
usage: main.py [-h] [-t TRAJECTORIES] [-d] [-v] [--length LENGTH] [-k K] [-l L]
               [-c {katz,degree,information,eigenvector,closeness,betweenness,
               accessibility,expected_force,expected_force_nolog,weighted_deg}]
               [-p PLOT] [--heatmap HEATMAP] [--start START] [--end END]
               [--coords COORDS]
               [--time TIME] [--curves] [--lost_threshold LOST_THRESHOLD]
               [--old_def_lost] graph_file

Computes a 2-approximation of the vertex cover then uses centrality to
determine which nodes are important waypoints where sensors should be placed

positional arguments:
  graph_file            Path to a graph file formatted like "node space node"

optional arguments:
  -h, --help            show this help message and exit
  -t TRAJECTORIES, --trajectories TRAJECTORIES
                        Path to a pickle trajectories file containing a list of
                        lists of tuples of two
                        ints
  -d, --debug           Will check the solution is correct once finished
  -v, --verbose         Turn on verbosity
  --length LENGTH       Length of the shortest paths for the accessibility
                        centrality
  -k K                  Minimal distance between sensors
  -l L                  Area of effect of sensors in metres
  -c {katz,degree,information,eigenvector,closeness,betweenness,accessibility,
      expected_force,expected_force_nolog,weighted_deg}, --centrality {katz,
      degree,information,eigenvector,closeness,betweenness,accessibility,
      expected_force,expected_force_nolog,weighted_deg}
                        Centrality type. It can be one of: katz, information,
                        degree, eigenvector, closeness, betweenness
                        accessibility expected_force or weighted_deg
  -p PLOT, --plot PLOT  File where the map will be plotted. If no extension is
                        given, defaults to png.
                        Supported formats include png, jpg, pdf, svf, pgf.
  --heatmap HEATMAP     Name of the file where the heatmap will be stored
  --start START         Beginning of the time window to consider for the
                        heatmap.
  --end END             End of the time window to consider for the heatmap
  --coords COORDS       File containing the geo-coordinates of all nodes of the
                        network
  --time TIME           File containing a dictionnary matching the nodes with a
                        list of timestamps at which they are visited
  --curves              Draw curves to analyze the results
  --lost_threshold LOST_THRESHOLD
                        Threshold below which trajectories are considered lost
  --old_def_lost        Triggers the old definition of lost, which an absolute
                        number of time a trajectory has to be seen to be
                        considered reconstructible. When turned off, the new
                        definition applies, with which a trajectory has to be
                        seen a certain percentage of time to be considered
                        reconstructible. That is, if a trajectory has x percent
                        of its vertices seen, it is considered reconstructible.
                        x being set with
  --lost_threshold
```

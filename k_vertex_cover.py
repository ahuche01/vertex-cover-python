#!/usr/bin/python3

# Get rid of deep copies

""" Tentative implementation of Bar-Yehuda and Even's 5/3-approximation for the
vertex cover problem on planar graphs from
https://doi.org/10.1145/800070.802205, with a heuristic for the k-vertex cover
problem"""

import logging
import queue
# import cProfile
# import pstats
import copy
import pickle
import collections
import random
#  import datetime
from math import exp, log, ceil
import multiprocessing
import numpy as np  # type: ignore
# sudo apt install libproj-dev proj-data proj-bin libgeos-dev
import cartopy.crs as ccrs  # type: ignore
from cartopy.io.img_tiles import OSM  # type: ignore
import matplotlib.pyplot as plt  # type:ignore
import networkx  # type: ignore
from networkx.algorithms.approximation import vertex_cover  # type: ignore
from rectangles import (__square_to_set__, haversine_distance, __get_coords__)
from linear_combination import combination
# import guppy


class Graph:
    """Graph class with methods for computing an approximation of vertex
    cover, along with a heuristic for the k-vertex cover problem."""

    def __init__(self, file):
        logging.log(25, "Importing %s…", file)
        self.file = file
        self.graph = networkx.read_edgelist(file, nodetype=int)
        self.graph_bkp = networkx.Graph()
        self.vertex_cover = set()
        # self.naive_vc = set()
        self.k_vertex_cover = set()
        self.trajectories = list()
        self.average_degree = int()

    def import_trajectories(self, path):
        """Imports trajectories that are to be checked against vertex cover"""
        routes = open(path, 'rb')
        self.trajectories = pickle.load(routes)
        routes.close()

    def report_trajectories(self, old_def_lost, var_l, val_k, coords_file):
        """Reports how many times trajectories are covered by the vertex
        cover. A trajectory is considered covered when it goes within distance
        var_l of a node of self.k_vertex_cover.

        Produces report = defaultdict(<class 'int'>, {110: 3149, 94: 6048}
        means 3149 trajectories have been covered 110 times and 6048
        trajectories have been covered 94 times.

        report_percent is a dictionnary in which the index is the percentage of
        nodes covered and the value is a list of trajectory whose node are
        covered at index%. report_percent[50] is the list of trajectories in
        which ]49, 50] % of the nodes are seen.
        """
        # If k = 0, consider self.vertex_cover, because self.k_vertex_cover
        # hasn't been built
        if not val_k:
            self.k_vertex_cover = self.vertex_cover
        report = collections.defaultdict(list)
        report_percent = collections.defaultdict(list)

        coords = __get_coords__(coords_file)
        # The set nodes_covered is the set of nodes that are covered by the
        # sniffers
        nodes_covered = set()
        for vertex_u in self.k_vertex_cover:
            for vertex_v in self.graph:
                if (vertex_u in coords and vertex_v in coords and
                        haversine_distance(coords[vertex_v][0],
                                           coords[vertex_v][1],
                                           coords[vertex_u][0],
                                           coords[vertex_u][1]) < var_l/1000):
                    nodes_covered.add(vertex_v)
        nodes_covered = nodes_covered.union(self.k_vertex_cover)

        # Checks if trajectories cross the set of covered nodes
        for (i, traj) in enumerate(self.trajectories):
            covered = 0
            for edge in traj:
                if edge[0] in nodes_covered:
                    covered += 1
            # Also checks the last node of the trajectory
            if traj[-1][1] in nodes_covered:
                covered += 1
            report[covered].append(traj)
            # len(trajectory) + 1 because we are comparing nodes with edges,
            # which is one less than the number of nodes.
            report_percent[ceil(covered/(len(traj)+1)*100)].append(traj)
            # print("covered", covered, "times. Length", len(trajectory))
            # So I don't get bored
            if i % 100000 == 0:
                logging.log(25, "%d out of %d trajectories processed.", i,
                            len(self.trajectories))
        if old_def_lost:
            return report
        return report_percent

    def report_vc(self, k=0):
        """ Reports how many trajectories go through each node of the vertex
        cover. """
        if k == 0:
            self.k_vertex_cover = self.vertex_cover
        report = collections.defaultdict(int)
        for traj in self.trajectories:
            for node1, _ in traj:
                if node1 in self.k_vertex_cover:
                    report[node1] += 1
            if traj[-1][1] in self.k_vertex_cover:
                report[traj[-1][1]] += 1
        return report

    def size_area(self, coords_file):
        """ Returns the size in km² of the area spanned by the graph """
        coords = __get_coords__(coords_file)
        extents = [min([a[1] for b, a in coords.items() if b in self.graph]),
                   max([a[1] for b, a in coords.items() if b in self.graph]),
                   min([a[0] for b, a in coords.items() if b in self.graph]),
                   max([a[0] for b, a in coords.items() if b in self.graph])]
        return (haversine_distance(extents[0], 0, extents[1], 0) *
                haversine_distance(extents[2], 0, extents[3], 0))

    def __triangles_itai_rodeh__(self):
        """Itai and Rodeh's algorithm for finding triangles in a graph as used
        by the article.
        It seems to be the same approach as the naive algorithm. It just takes
        edges in a certain order, given by a spanning tree. In the graphs that
        I used, the naive implementation is faster. Could it be because my
        graphs are sparse and computing neighbours is cheap? Cheaper than
        computing spanning trees it seems."""
        triangles = set()
        chosen = set()

        cpt = 0
        nedges = len(self.graph.edges())
        while self.graph.edges():
            # For each connected component
            for component in networkx.connected_components(self.graph):
                logging.log(15, "Component: %s", str(component))
                tree = networkx.dfs_tree(self.graph, component.pop())
                # For each edge of the spanning tree over the given component
                for vertex_u, vertex_v in tree.edges():
                    logging.log(15, "Considering edge (%d, %d) of tree %s",
                                vertex_u, vertex_v, str(tree.edges()))
                    # Get a disjoint triangle
                    self.__get_triangle__(vertex_u, vertex_v, triangles,
                                          chosen)
                    # Then remove the edge from the graph
                    self.graph.remove_edge(vertex_u, vertex_v)
                    cpt += 1
                    # Just so I don't get bored
                    if cpt % 100 == 0:
                        logging.log(25, ("%d edges out of %d, %d triangles so "
                                         "far"),
                                    cpt, nedges, len(triangles))

        self.graph = networkx.read_edgelist(self.file, nodetype=int)
        # Adding triangles to the vertex cover
        for triangle in triangles:
            self.vertex_cover = self.vertex_cover.union(triangle)
        # Removing triangles from the graph
        self.__rm_triangles__(triangles)

    def __get_triangle__(self, vertex_u, vertex_v, triangles, chosen):
        """Gets a triangle from the current tree-edge if possible"""
        for neigh_u in self.graph.neighbors(vertex_u):
            # The nodes of the triangle must not be chosen already. We need
            # disjoint triangles
            if (neigh_u in self.graph.neighbors(vertex_v) and
                    neigh_u not in chosen and
                    vertex_u not in chosen and
                    vertex_v not in chosen):
                logging.log(15, "%d both in %s and %s", neigh_u,
                            str(list(self.graph.neighbors(vertex_u))),
                            str(list(self.graph.neighbors(vertex_v))))
                triangles.add(frozenset({neigh_u, vertex_u, vertex_v}))
                chosen.add(neigh_u)
                chosen.add(vertex_u)
                chosen.add(vertex_v)
                return

    def __triangles_naive__(self):
        """Precondition : Undirected graph, no self loops, no multi-edges Get
        all disjoint triangles with a naive algorithm that analyzes common
        neighbours of both ends of an edge. Then removes them. The initial
        article used Itai and Rodeh's algorithm, not this one."""
        triangles = set()
        cpt = 0
        nedges = len(self.graph.edges())
        for vertex_u, vertex_v in self.graph.edges():
            logging.log(15, "Chosen edge (%d,%d)", vertex_u, vertex_v)
            # I didn't know there could be self loops in road networks
            if vertex_u == vertex_v:
                logging.log(15, "SELF LOOP (%d,%d)", vertex_u, vertex_v)
                raise ValueError("Self loop %d" % vertex_u)

            neigh_u = set(self.graph.neighbors(vertex_u))
            logging.log(15, "Neighbours of %d: %s", vertex_u, str(neigh_u))

            for neigh in self.graph.neighbors(vertex_v):
                if neigh in neigh_u:
                    logging.log(15, "%d in edges %s", neigh, str(neigh_u))
                    # Additional check to consider disjoint triangles only
                    if ({neigh, vertex_u, vertex_v} not in triangles and
                            vertex_u not in self.vertex_cover and
                            vertex_v not in self.vertex_cover and
                            neigh not in self.vertex_cover):

                        logging.log(15, "adding (%d,%d,%d) to triangles",
                                    neigh, vertex_u, vertex_v)
                        triangles.add(frozenset({neigh, vertex_u, vertex_v}))
                        logging.log(15, "adding %d to vc", neigh)
                        self.vertex_cover.add(neigh)
                        logging.log(15, "adding %d to vc", vertex_u)
                        self.vertex_cover.add(vertex_u)
                        logging.log(15, "adding %d to vc", vertex_v)
                        self.vertex_cover.add(vertex_v)
                else:
                    logging.log(15, "%d not in edges %s", neigh, str(neigh_u))

            # Just so that I don't get bored waiting
            cpt += 1
            if cpt % 1000 == 0:
                logging.log(25, "%d out of %d. %d triangles so far",
                            cpt, nedges, len(triangles))

        self.__rm_triangles__(triangles)

    def __rm_triangles__(self, triangles):
        """Removes all triangles in the set triangles"""
        for triangle in triangles:
            triangle_list = list(triangle)
            self.__rm_triangle__(triangle_list[0],
                                 triangle_list[1],
                                 triangle_list[2])

    def __rm_triangle__(self, vertex_a, vertex_b, vertex_c):
        """Removes a triangle given its three vertices"""
        self.graph.remove_node(vertex_a)
        self.graph.remove_node(vertex_b)
        self.graph.remove_node(vertex_c)

    def __distance_2_neighbours__(self, neighbors, vertex):
        """Get neighbours that are at distance 2 of vertex"""
        nneighbors = set()
        for neigh in neighbors:
            nneighbors = nneighbors.union(self.graph.neighbors(neigh))
        nneighbors.discard(vertex)
        return nneighbors

    def __build_priority_queue__(self):
        """Build priority queue to get min degree nodes"""
        degrees = queue.PriorityQueue()
        for node, degree in self.graph.degree():
            degrees.put((degree, node))
        return degrees

    def __process_matching__(self, neighbors, nneighbors, vertex):
        """Matching construction.
            - Match each neighbour with a distance 2 neighbour.
            - Add all neighbours to the vertex cover.
            - Add only matched distance 2 neighbours to the vertex cover.
            - Remove all neighbours from the graph.
            - Remove only matched distance 2 neighbours from the graph.
        """
        vertex_d = neighbors.pop()
        for i in neighbors:
            logging.log(15, "Adding %d to VC", i)
            self.vertex_cover.add(i)
            matched = False
            for j in nneighbors:
                if j in self.graph.neighbors(i) and not matched:
                    logging.log(15, "Adding %d to VC", j)
                    self.vertex_cover.add(j)
                    self.graph.remove_node(j)
                    matched = True
            self.graph.remove_node(i)

        logging.log(15, "Adding %d to VC", vertex_d)
        # Adding the last neighbour of v to the vertex cover and
        # removing it from the graph.
        self.vertex_cover.add(vertex_d)
        self.graph.remove_node(vertex_d)
        # v doesn't need to be in the vertex cover since its neighbours
        # are.
        self.graph.remove_node(vertex)

    def __get_min_degree_node__(self, degrees):
        """Pick a minimum degree node. Don't pick a node from the priority
        queue that is already in the vertex cover"""
        degree, vertex = degrees.get()
        while vertex in self.vertex_cover:
            degree, vertex = degrees.get()
        return degree, vertex

    def __phase2__(self):
        """Phase 2 of Bar-Yehuda and Even's algorithm
        While nodes remain:
          - Get a minimum degree node v, get all its neighbours X and all its
          neighbours at distance 2 Y.
          - Build a matching between X (minus one element) and a subset of Y,
          Y'
          - Add X and Y' to the vertex cover.
          - Remove X, Y' and v from the graph"""
        # profile = cProfile.Profile()
        # profile.enable()

        degrees = self.__build_priority_queue__()

        cpt = 0
        degree_sum = 0
        while self.graph.nodes():

            degree, vertex = self.__get_min_degree_node__(degrees)
            degree_sum += degree

            # If v has degree 0, there is no point in using it in the vertex
            # cover since it covers nothing. Going through the else branch
            # below would do the same. I thought it might be faster not to go
            # through it
            if not self.graph.degree(vertex):
                self.graph.remove_node(vertex)
                logging.log(15, "Degree 0")
            elif self.graph.degree(vertex) == 1:
                neighbors = set(self.graph.neighbors(vertex))
                self.__process_matching__(neighbors, set(), vertex)
                logging.log(15, "Degree 1")
            else:
                logging.log(15, "Choosing node %d", vertex)

                # Get neighbours and distance two neighbours
                neighbors = set(self.graph.neighbors(vertex))
                nneighbors = self.__distance_2_neighbours__(neighbors, vertex)

                logging.log(15, "Neighbors of %d: %s", vertex, str(neighbors))
                logging.log(15, "2nd neighbors of %d: %s", vertex,
                            str(nneighbors))

                self.__process_matching__(neighbors, nneighbors, vertex)

            # Just so that I don't get bored waiting
            cpt += 1
            if cpt % 1000 == 0:
                logging.log(25, ("%d edges still uncovered. "
                                 "%d nodes left. Current deg: %d"),
                            len(self.graph.edges()), len(self.graph.nodes()),
                            degree)

        self.average_degree = degree_sum/cpt
        # profile.disable()
        # ps = pstats.Stats(profile)
        # ps.print_stats()

    def __get_biggest_cc__(self):
        """Leaves only the biggest connected component of the graph"""
        logging.log(25, "Computing biggest cc…")
        # Somehow this one liner is a little longer
        # self.graph = self.graph.subgraph(
        #                 max(networkx.connected_components(self.graph))).copy()
        biggest_cc = max(networkx.connected_components(self.graph),
                         key=len)
        # To get the list of nodes outside the biggest cc:
        # [node for node in self.graph if node not in biggest_cc]
        # The list of coords of nodes outside of the biggest cc:
        # coords = __get_coords__(args.coords)
        # [coords[node] for node in self.graph if node not in biggest_cc]
        nodes = copy.deepcopy(self.graph.nodes())
        logging.log(25, "Biggest connected component size %d",
                    len(biggest_cc))
        for node in nodes:
            if node not in biggest_cc:
                self.graph.remove_node(node)

    def k_vertex_cover_wrapper(self, args):
        """Wrapper for k_vertex_cover"""
        if args.k:
            self.__get_biggest_cc__()
        self.graph_bkp = copy.deepcopy(self.graph)
        # Use Bar-Yehuda and Even
        self.__triangles_naive__()
        # self.__triangles_itai_rodeh__()
        self.__phase2__()
        # Or a 2-approximation
        # self.naive_approx()
        # Or start from the full graph
        # self.vertex_cover = set(self.graph.nodes())
        # Or randomly select a certain number of nodes
        # print(args.k)
        # self.vertex_cover = set(random.sample(
        #     set(self.graph.nodes()), k=args.k))
        # k=int(len(self.graph.nodes())/2)))
        logging.log(25, "Copying graphs…")
        self.graph = copy.deepcopy(self.graph_bkp)
        if args.plot:
            self.prune_vertex_cover()
        if args.k:
            logging.log(25, "Removing nodes from the VC")
            if args.coords:
                self.__compute_k_vertex_cover__(args)
            else:
                logging.log(30, "A coordinate file must be provided to compute"
                            " the nodes within reach of the sniffers")
        else:
            self.k_vertex_cover = self.vertex_cover

    def __get_centrality__(self, centrality, length=4):
        """Computes the requested centrality"""
        centrality_dict = dict()
        if centrality == "betweenness":
            # Takes an hour so result is cached.
            logging.log(25, "Using betweenness centrality")
            # betweenness = open("Data/Cologne/betweenness", 'rb')
            # centrality_dict = pickle.load(betweenness)
            # betweenness.close()
            centrality_dict = networkx.algorithms.betweenness_centrality(
                self.graph)
        elif centrality == "closeness":
            logging.log(25, "Using closeness centrality")
            # Takes 40 minutes so result is cached.
            # closeness = open("Data/Cologne/closeness", 'rb')
            # centrality_dict = pickle.load(closeness)
            # closeness.close()
            centrality_dict = networkx.algorithms.closeness_centrality(
                self.graph)
        elif centrality == "eigenvector":
            # logging.log(25, "Using eigenvector centrality")
            centrality_dict = networkx.algorithms.\
                eigenvector_centrality_numpy(self.graph)
        elif centrality == "degree":
            # logging.log(25, "Using degree centrality")
            centrality_dict = networkx.algorithms.degree_centrality(self.graph)
        elif centrality == "information":
            # logging.log(25, "Using information centrality")
            # Takes 15 minutes so result is cached.
            # info = open("info", 'rb')
            # centrality_dict = pickle.load(info)
            # info.close()
            centrality_dict = networkx.algorithms.information_centrality(
                self.graph)
        elif centrality == "katz":
            # logging.log(25, "Using Katz centrality")
            centrality_dict = networkx.algorithms.katz_centrality(self.graph)
        elif centrality == "accessibility":
            # logging.log(25, "Using accessibility node influence")
            count = 0
            for node in self.graph.nodes():
                centrality_dict[node] = self.accessibility(node, length)
                count += 1
                if count % 100 == 0:
                    logging.log(25, "%d %d", count, length)
            # Uncomment to dump normalized centrality to a file
            # maximum = max(centrality_dict.values())
            # for key in centrality_dict:
            #     centrality_dict[key] /= maximum
            # routes = open("accessibility_fixed_31614_{}".format(length),
            #               'wb')
            # pickle.dump(centrality_dict, routes)
            # routes.close()
        elif centrality == "expected_force":
            # logging.log(25, "Using expected force node influence")
            for node in self.graph.nodes():
                centrality_dict[node] = self.expected_force(node)
            # Uncomment to dump normalized centrality to a file
            # maximum = max(centrality_dict.values())
            # for key in centrality_dict:
            #     centrality_dict[key] /= maximum
            # routes = open("expected_force_fixed_log", 'wb')
            # pickle.dump(centrality_dict, routes)
            # routes.close()
        # elif centrality == "expected_force_nolog":
        #     logging.log(25, "Using expected force node influence")
        #     for node in self.graph.nodes():
        #         centrality_dict[node] = self.expected_force_nolog(node)
        #     minimum = min(centrality_dict.values())
        #     # Negatives values, so they are shifted to positive
        #     for key in centrality_dict:
        #         centrality_dict[key] -= minimum
        #     # Uncomment to dump normalized centrality to a file
        #     # maximum = max(centrality_dict.values())
        #     # for key in centrality_dict:
        #     #     centrality_dict[key] /= maximum
        #     # routes = open("expected_force_fixed_log", 'wb')
        #     # pickle.dump(centrality_dict, routes)
        #     # routes.close()
        elif centrality == "combination":
            # coeffs = combination(self.centralities_most_traj())
            # Bologne = [0.1649337788044182, 0.18613649732046342,
            # 0.10961297103852191, 0.16013021517201706, 0.053392401288955706,
            # 0.10207419039351406, 0.06462129290205261, 0.1590986530800569]
            # Cologne = [0.11573877276621208, 0.12021006067343261,
            # 0.16911288584561365, 0.11741347374687758, 0.14396614700911906,
            # 0.14157721153729527, 0.07431503547191079, 0.11766641294953904]
            # These coefficients are the average of the above "coeffs" across
            # several datasets. For now only Bologne and Cologne.
            # coeffs = [0.22287288095314609, 0.2420872784090565,
            #           0.018337695899733643, 0.057918070050244325,
            #           -0.06522448258161835, 0.1922230634607931,
            #           0.11413045319329185, 0.21765504061535285]
            coeffs = [0.19820566216842117, 0.21327830933366432,
                      0.2239193713648746, 0.1974785813328861,
                      0.17066234765359692, 0.1926143067340523,
                      0.10662568192293709, 0.1972157394895675]
            katz = self.__get_centrality__("katz")
            degr = self.__get_centrality__("degree")
            info = self.__get_centrality__("information")
            betw = self.__get_centrality__("betweenness")
            clos = self.__get_centrality__("closeness")
            acce = self.__get_centrality__("accessibility")
            eige = self.__get_centrality__("eigenvector")
            expe = self.__get_centrality__("expected_force")
            for node in self.graph.nodes():
                centrality_dict[node] = (
                    coeffs[0] * katz[node] +
                    coeffs[1] * degr[node] +
                    coeffs[2] * info[node] +
                    coeffs[3] * betw[node] +
                    coeffs[4] * clos[node] +
                    coeffs[5] * acce[node] +
                    coeffs[6] * eige[node] +
                    coeffs[7] * expe[node]
                )
        return centrality_dict

    def __rebuild_lists__(self, centrality, max_centrality):
        """ Rebuilds the nodes and probability lists with the remaining nodes
        and their associated probability """
        nodes = list()
        proba = list()
        for _, (key, value) in enumerate(centrality.items()):
            if key in self.k_vertex_cover:
                nodes.append(key)
                # Normalized. When recomputing centrality, avoids
                # dividing by zero.
                # if not max_centrality:
                #     proba.append(value)
                # else:
                #     proba.append(value/max_centrality)
                proba.append(value/max_centrality)
        return proba, nodes

    def __rebuild_lists_weights__(self, max_centrality):
        """ Rebuilds the nodes and probability lists with the remaining nodes
        and their associated probability """
        nodes = list()
        proba = list()
        for node in self.graph.nodes:
            weight = 0
            if node in self.k_vertex_cover:
                nodes.append(node)
                for neighbour in self.graph[node]:
                    weight += self.graph[node][neighbour]['weight']
                proba.append(weight)
                # Normalized. When recomputing centrality, avoids
                # dividing by zero.
                # if not max_centrality:
                #     proba.append(value)
                # else:
                #     proba.append(value/max_centrality)
        # max_proba = max(proba)
        # proba = [p/max_proba for p in proba]
        proba = [p/max_centrality for p in proba]
        return proba, nodes

    def __compute_k_vertex_cover__(self, args):
        """Computes a vertex cover with an area of effect or radius k.
        The graph must be as it was before computing the vertex cover!"""
        self.k_vertex_cover = copy.deepcopy(self.vertex_cover)
        chosen = set()
        cpt = 0

        if args.centrality in ["weighted_deg"]:
            max_centrality = max(sorted(self.__rebuild_lists_weights__(1)[0]))
            initial_proba = sorted(
                self.__rebuild_lists_weights__(max_centrality)[0])
        elif args.centrality != "":
            centrality = self.__get_centrality__(args.centrality, args.length)
            # min_centrality = min(centrality.values())
            max_centrality = max(centrality.values())
            initial_proba = sorted(
                self.__rebuild_lists__(centrality, max_centrality)[0])
        while self.k_vertex_cover:

            if args.centrality != "":
                # Uncomment to recompute centrality each time. Won't work for
                # imported centralities.
                # centrality = self.__get_centrality__()
                # max_centrality = max(centrality.values())

                # Rebuilding lists each time because I haven't figured out a
                # better way yet.
                if args.centrality in ["weighted_deg"]:
                    proba, nodes = self.__rebuild_lists_weights__(
                        max_centrality)
                else:
                    proba, nodes = self.__rebuild_lists__(centrality,
                                                          max_centrality)

                # Other way so I don't have to recompute nodes and proba. Is it
                # faster?
                # vertex_u = random.choices([b for (b,_) in a],
                #                            weights=[b for (_, b) in a])[0]
                # for b, c in a:
                #     if b == vertex_u:
                #         a.remove((b,c))

                # Either pick a random sample based on centrality
                # vertex_u = random.choices(nodes, weights=proba)[0]
                # or just pick the most central node:
                vertex_u = nodes[proba.index(max(proba))]
                # Or pick a random node
                # vertex_u = random.choices(nodes)[0]

                # Get the index of vertex_u among all the nodes, ordered by
                # their centrality. Do "1-" to get a small value when
                # centrality is big.
                # print(len(initial_proba), initial_proba.index(max(proba)))
                cent_level = 1 - (initial_proba.index(max(proba))
                                  + 1)/len(initial_proba)
                # cent_level2 = (initial_proba.index(max(proba))
                #                   + 1)/len(initial_proba)
                # print(cent_level, cent_level2)
                # Or
                # cent_level = (len(initial_proba) /
                #               initial_proba.index(max(proba)))

                self.k_vertex_cover.remove(vertex_u)
            else:
                # Or just use an arbitrary one:
                # vertex_u = self.k_vertex_cover.pop()
                # Better yet, a random one:
                vertex_u = random.choice(tuple(self.k_vertex_cover))
                self.k_vertex_cover.remove(vertex_u)

            chosen.add(vertex_u)
            # Getting the graph centered at vertex_u, of radius k-1 so that the
            # vertex in the vertex cover at distance k can cover vertex_u
            # Include all neighbors of distance<=radius from n
            # Reverted to k
            if args.centrality != "":
                # int((k-maximum*k)*k))
                # print(cent_level, round(cent_level * args.k))
                k_neighbours = networkx.ego_graph(self.graph,
                                                  vertex_u,
                                                  # args.k)
                                                  round(cent_level*args.k))
            else:
                k_neighbours = networkx.ego_graph(self.graph, vertex_u, args.k)
            for vertex_v in k_neighbours.nodes():
                if vertex_v in self.k_vertex_cover:
                    self.k_vertex_cover.remove(vertex_v)
                    logging.log(15, "Removing node %d from the vertex cover",
                                vertex_v)
            # Removing nodes within l meters of the chosen node vertex_u
            self.__remove_nodes_within_radius__(args, vertex_u)
            # So I don't get bored
            cpt += 1
            if cpt % 100 == 0:
                logging.log(25, "%d nodes remaining", len(self.k_vertex_cover))
        self.k_vertex_cover = chosen

    def __remove_nodes_within_radius__(self, args, vertex_u):
        """ Removing nodes within l meters of the chosen node vertex_u """
        coords = __get_coords__(args.coords)
        to_prune = set()
        for vertex_v in self.k_vertex_cover:
            if (vertex_v in coords and vertex_u in coords and
                    haversine_distance(coords[vertex_v][0],
                                       coords[vertex_v][1],
                                       coords[vertex_u][0],
                                       coords[vertex_u][1])
                    < args.l/1000):
                to_prune.add(vertex_v)
                # self.k_vertex_cover.remove(vertex_v)
                logging.log(15, "Removing node %d from the vertex cover",
                            vertex_v)
        self.k_vertex_cover.difference_update(to_prune)

    def correctness(self, k):
        """Checks the correctness of the k-vertex cover"""
        nb_vertices = len(self.graph.nodes())
        cpt = 0
        for vertex_u in self.graph.nodes():
            # This needs to be set to k+1 because sometimes we make nodes k+1
            # covered.
            # Consider the graph:        o -- o -- o -- o -- o
            # A vertex cover would be:   o -- x -- o -- x -- o
            # A 2-vertex cover would be: o -- x -- o -- o -- o
            # Which is actually a 3-vertex cover.
            # When picking a node u, then removing all nodes within distance k
            # in the vertex cover, we might remove a node at distance exactly
            # k, which covered its neighbours only, one of which might have
            # been at distance k+1.
            k_neighbours = networkx.ego_graph(self.graph, vertex_u, k+1)
            self.__k_covered__(k_neighbours, vertex_u)
            cpt += 1
            if cpt % 1000 == 0:
                logging.log(25, "%d out of %d nodes processed.", cpt,
                            nb_vertices)

    def __k_covered__(self, k_neighbours, vertex_u):
        """Checks if the k_neighbours have a node in the k_vertex_cover"""
        for vertex_v in k_neighbours.nodes():
            if vertex_v in self.k_vertex_cover:
                return
        logging.log(25, "%d not covered by any node", vertex_u)

    def naive_approx(self):
        """The vertex cover approximation as implemented by the networkx
        library. From https://doi.org/10.1016/S0304-0208(08)73101-3 (A
        local-ratio theorem for approximating the weighted vertex cover
        problem.)"""
        self.vertex_cover = vertex_cover.min_weighted_vertex_cover(self.graph)

    def centrality_sample(self, k, centrality, threshold):
        """ Picks threshold nodes following a distribution given by some
        centrality """
        centrality = self.__get_centrality__(centrality)
        max_centrality = max(centrality.values())
        cpt = 0

        while len(self.k_vertex_cover) < threshold:
            nodes, proba = list(), list()
            for _, (key, value) in enumerate(centrality.items()):
                if key in self.graph.nodes():
                    nodes.append(key)
                    proba.append(value/max_centrality)
            vertex_u = random.choices(nodes)[0]  # , weights=proba)[0]

            self.k_vertex_cover.add(vertex_u)
            # Getting the graph centered at vertex_u, of radius k-1 so that the
            # vertex in the vertex cover at distance k can cover vertex_u
            k_neighbours = networkx.ego_graph(self.graph, vertex_u, k)
            for vertex_v in k_neighbours.nodes():
                if vertex_v in self.graph.nodes():
                    self.graph.remove_node(vertex_v)
                    logging.log(15, "Removing node %d from the graph",
                                vertex_v)
            # So I don't get bored
            cpt += 1
            if cpt % 1 == 0:
                logging.log(25, "%d out of %d nodes processed.",
                            len(self.k_vertex_cover), threshold)
        self.graph = networkx.read_edgelist(self.file, nodetype=int)

    def get_zone_heat(self, extents, resolution, node_heat, coords_file):
        """ Divides the zone within extents, into resolution² subzones, then
        computes subzones heat based on node_heat dictionnary. """
        coords = __get_coords__(coords_file)

        pad_lon = (extents[1]-extents[0])/resolution
        lons = np.linspace(extents[0], extents[1], resolution)

        pad_lat = (extents[3]-extents[2])/resolution
        lats = np.linspace(extents[2], extents[3], resolution)

        # O(n^3) version
        # heats = []
        # count = 0
        # for i in lons:
        #     heat_lon = []
        #     for j in lats:
        #         heat = 0
        #         for node in self.graph:
        #             if (i <= coords[node][1] < i+pad_lon
        #                     and j <= coords[node][0] < j+pad_lat):
        #                 heat += node_heat[node]
        #         heat_lon.append(heat)
        #         count += 1
        #         if count % 100 == 0:
        #             logging.log(25, "%d out of %d generated.", count,
        #                         len(lons)*len(lats))
        #     heats.append(heat_lon)

        # O(n) version
        heats_matrix = [[0]*len(lons) for _ in range(len(lats))]
        for node in self.graph:

            lat_index = (coords[node][0]-extents[2])/pad_lat
            if lat_index == int(lat_index):
                lat_index -= 1
            lat_index = int(lat_index)

            lon_index = (coords[node][1]-extents[0])/pad_lon
            if lon_index == int(lon_index):
                lon_index -= 1
            lon_index = int(lon_index)

            heats_matrix[lat_index][lon_index] += node_heat[node]

        return lons, lats, heats_matrix

    def plot_heatmap_ugly(self, node_heat, file_save, resolution, coords_file):
        """ Plots a heatmap or all trajectories """
        coords = __get_coords__(coords_file)
        plt.figure(figsize=(3.5, 4))
        mapp = plt.axes(projection=OSM().crs)
        # (min_lon, max_lon, min_lat, max_lat))
        extents = (min([a[1] for b, a in coords.items() if b in self.graph]),
                   max([a[1] for b, a in coords.items() if b in self.graph]),
                   min([a[0] for b, a in coords.items() if b in self.graph]),
                   max([a[0] for b, a in coords.items() if b in self.graph]))
        mapp.set_extent(extents, crs=ccrs.PlateCarree())
        # 20 doesn't work, 19 yields some 404, 16 fills my memory at some point
        mapp.add_image(OSM(), 14)
        logging.log(25, "Generating plot…")

        # https://matplotlib.org/stable/tutorials/colors/colormaps.html
        plt.pcolormesh(*self.get_zone_heat(extents, resolution, node_heat,
                                           coords_file),
                       alpha=0.5, transform=ccrs.PlateCarree(),
                       cmap='coolwarm')  # shading='gouraud')

        # dpi 1400 for whole map
        # dpi 300 for downtown
        plt.savefig(file_save, dpi=300)

    def plot_heatmap(self, heatmap_file, coords_file, uncovered=None):
        """ Plots the vertex cover and the uncovered trajectories on the map
        """
        # gps = get_gps(self.k_vertex_cover)
        if uncovered is None:
            uncovered = []
        coords = __get_coords__(coords_file)
        # Good for beamer pgf plot
        # plt.figure(figsize=(3.15, 3.2))
        # Good for jpg plot
        plt.figure(figsize=(3.5, 4))
        mapp = plt.axes(projection=OSM().crs)
        # (min_lon, max_lon, min_lat, max_lat))
        extents = [min([a[1] for b, a in coords.items() if b in self.graph]),
                   max([a[1] for b, a in coords.items() if b in self.graph]),
                   min([a[0] for b, a in coords.items() if b in self.graph]),
                   max([a[0] for b, a in coords.items() if b in self.graph])]
        mapp.set_extent(tuple(extents))

        c = collections.defaultdict(int)
        for traj in self.trajectories:
            for (u, v) in traj:
                c[u] += 1
            c[traj[-1][-1]] += 1
        d = max(c.values())
        for i, j in c.items():
            c[i] /= d

        # Second argument is zoom: more means more detailed
        mapp.add_image(OSM(), 14)
        for a in c.keys():
            mapp.plot(coords[a][1],
                      coords[a][0],
                      "o",
                      # Colour range from dark to red
                      color=(c[a], 0, 0),
                      # color=plt.cm.Reds(c[a]),
                      # color=plt.cm.autumn(c[a]),
                      # This is just to give bigger maps smaller markers, and
                      # make important vertices bigger (range 0.8 to 0.8+0.7)
                      markersize=((0.03 + (c[a]/(1/0.35))) /
                                  (extents[3]-extents[2]+extents[1]-extents[0])),
                      # For downtown:
                      # markersize=3,
                      # For whole map
                      # markersize=0.1,
                      transform=ccrs.Geodetic())
        plt.tight_layout()
        plt.savefig(heatmap_file, dpi=1200)

    def plot_cover(self, args, uncovered=None):
        """ Plots the vertex cover and the uncovered trajectories on the map
        """
        # gps = get_gps(self.k_vertex_cover)
        if uncovered is None:
            uncovered = []
        coords = __get_coords__(args.coords)
        # Good for beamer pgf plot
        # plt.figure(figsize=(3.15, 3.2))
        # Good for jpg plot
        plt.figure(figsize=(3.5, 4))
        mapp = plt.axes(projection=OSM().crs)
        # (min_lon, max_lon, min_lat, max_lat))
        extents = [min([a[1] for b, a in coords.items() if b in self.graph]),
                   max([a[1] for b, a in coords.items() if b in self.graph]),
                   min([a[0] for b, a in coords.items() if b in self.graph]),
                   max([a[0] for b, a in coords.items() if b in self.graph])]
        size = 0.10 / (extents[3]-extents[2]+extents[1]-extents[0])
        mapp.set_extent(tuple(extents))
        # Second argument is zoom: more means more detailed
        mapp.add_image(OSM(), 14)
        mapp.plot([a[1] for b, a in coords.items()
                   if b in self.k_vertex_cover],
                  [a[0] for b, a in coords.items()
                   if b in self.k_vertex_cover],
                  "o",
                  color=(1, 0, 0),
                  # This is just to make bigger maps have smaller markers
                  markersize=(0.15 /
                              (extents[3]-extents[2]+extents[1]-extents[0])),
                  # For downtown:
                  # markersize=3,
                  # For whole map
                  # markersize=0.1,
                  transform=ccrs.Geodetic())

        unknown = 0
        total = 0
        count = 0
        # outbounds = 0
        for traj in uncovered:
            node1_bkp = -1
            for node1, node2 in traj:
                colour_r = (random.random(), random.random(), random.random())
                if node1 in coords and node2 in coords:
                    # if not (extents[0] <= coords[node1][1] < extents[1]
                    #         and extents[2] <= coords[node1][0] < extents[3]):
                    #     outbounds += 1
                    #     logging.log(25, "%d out of bounds: (%f, %f)", node1,
                    #                 coords[node1][0], coords[node1][1])
                    # elif not (extents[0] <= coords[node2][1] < extents[1]
                    #         and extents[2] <= coords[node2][0] < extents[3]):
                    #     outbounds += 1
                    #     logging.log(25, "%d out of bounds: (%f, %f)", node2,
                    #                 coords[node2][0], coords[node2][1])
                    if (extents[0] < coords[node1][1] < extents[1]
                            and extents[2] < coords[node1][0] < extents[3]
                            and extents[0] < coords[node2][1] < extents[1]
                            and extents[2] < coords[node2][0] < extents[3]):
                        mapp.plot([coords[node1][1], coords[node2][1]],
                                  [coords[node1][0], coords[node2][0]],
                                  color=colour_r,
                                  # Good for Downtown
                                  # linewidth=1,
                                  linewidth=size,
                                  # Good for whole map
                                  # linewidth=0.2,
                                  marker=',', markersize=3,
                                  transform=ccrs.Geodetic())
                        node1_bkp = node1
                elif node1 in coords and node2 not in coords:
                    node1_bkp = node1
                elif (node1 not in coords and node2 in coords
                      and node1_bkp != -1):
                    node1 = node1_bkp
                    mapp.plot([coords[node1][1], coords[node2][1]],
                              [coords[node1][0], coords[node2][0]],
                              color=colour_r,
                              # Good for Downtown
                              linewidth=1, marker=',', markersize=3,
                              # Good for whole map
                              # linewidth=0.2, marker=',', markersize=3,
                              transform=ccrs.Geodetic())
                elif (node1 not in coords and node2 in coords
                      and node1_bkp == -1):
                    unknown += 1
                total += 1
            count += 1
            if count % 10 == 0:
                logging.log(25, "%d out of %d trajectories processed.", count,
                            len(uncovered))
        logging.log(25, "%d coordinates unknown out of %d", unknown, total)
        # logging.log(25, "%d coordinates out of bounds out of %d",
        #             outbounds, total)

        if args.centrality == "accessibility":
            plt.title("Sensor locations for k=%d, l=%d, h=%d.\n%s centrality."
                      "k varying" % (args.k, args.l, args.length,
                                     args.centrality))
        else:
            plt.title("Sensor locations for k = %d, l = %d\n%s centrality."
                      " k varying" % (args.k, args.l, args.centrality))
        plt.tight_layout()
        plt.savefig(args.plot, dpi=1200)

    def prune_vertex_cover(self):
        """ Get rid of potential Added[On|Off]RampNode in the cover. There are
        66 of them in the TAPASCologne graph. I assume they can be safely
        removed since they are all of degree two and seem to be generated by
        SUMO. Experiments show that removing them has no effect on the number
        of trajectories measured.

        WARNING: This makes the vertex cover not completely covering."""
        logging.log(25, "Pruning vertex cover from AddedOnRampNodes…")
        mapping_file = open('mapping', 'rb')
        mapping = pickle.load(mapping_file)
        mapping_file.close()
        ramp_nodes = set()
        for i in self.vertex_cover:
            if i in mapping and "Added" in mapping[i]:
                ramp_nodes.add(i)
                logging.log(25, "%d is %s added node", i, mapping[i])
        self.vertex_cover.difference_update(ramp_nodes)
        logging.log(25, "Pruning done")

    def __avoiding_walks__(self, node, length, export=False):
        """ Builds the list of all self avoiding walks, sarting at node node,
        of length at most length."""
        walks = [[node]]
        length_remaining = length
        while length_remaining:
            length_remaining -= 1
            walks_tmp = list()
            # Will store the list of walks that have just been expanded since
            # they are just prefix of interesting paths.
            expanded = set()
            for walk in walks:
                # walks contains all walks generated so far
                if len(walk) == length - length_remaining:
                    for neigh in self.graph.neighbors(walk[-1]):
                        if neigh not in walk:
                            walks_tmp.append(walk + [neigh])
                            expanded.add(frozenset(walk))
            walks += walks_tmp
            # Comment the following line for old accessibility
            walks = [walk for walk in walks if frozenset(walk) not in expanded]
        # There is one more node than the length of the path
        # We don't need node in the walk
        # If we are exporting, return all walks, including those of shorter
        # length. It will be taken care of by the calling function. It avoids
        # recomputing walks.
        if export:
            return [walk for walk in walks]
        # If we want only the walks of length length. The paper used this for
        # road graphs as it represents a better model than using stationary
        # agents at the extremities of a network.
        return [walk for walk in walks if len(walk) == length+1]

    def __average_accessibility_node__(self, node, accessibility, length=20):
        """ Computes the average accessibility of a node. """
        walks = self.__avoiding_walks__(node, length, True)
        walks_length = dict()
        for i in range(length+1):
            walks_length[i] = list()
        for walk in walks:
            walks_length[len(walk)].append(walk)

        res = 0
        for len_path in walks_length:
            res += self.accessibility(node, len_path, walks_length[len_path])
        accessibility[node] = res/length
        logging.log(25, "%d out of %d", len(accessibility), len(self.graph))

    def export_average_accessibility(self, length=20):
        """ Computes and exports the average accessibility of all nodes. """
        manager = multiprocessing.Manager()
        accessibilities = manager.dict()
        pool = multiprocessing.Pool(multiprocessing.cpu_count()/2)
        # pool.map(partial(self.__average_accessibility_node__,
        #                  accessibility=return_dict, length=length),
        #          range(1, 10))
        pool.starmap(self.__average_accessibility_node__,
                     [(node, accessibilities, length) for node in self.graph])
        accessibilities = dict(accessibilities)
        coords_file = open("accessibilites_average_{}".format(length), 'wb')
        pickle.dump(accessibilities, coords_file)
        coords_file.close()
        return accessibilities

    def accessibility(self, node, length=4, walks=None):
        """ Computes the outward accessibility of a node, as defined in
        https://dx.doi.org/10.1016%2Fj.physleta.2008.10.069 """
        if walks is None:
            walks = self.__avoiding_walks__(node, length)
        nodes = [node for walk in walks for node in walk]
        # total = len(nodes)
        number_walks = len(walks)
        proba = collections.Counter(nodes)
        number_nodes = len(self.graph.nodes())
        # Dividing each p/number_walks by number_nodes+1 results in all
        # values being very close.
        return exp(-sum(((p/number_walks)) *
                        log((p/number_walks))
                        for p in proba.values()))/(number_nodes+1)

    # def old_accessibility(self, node, length=4, walks=None):
    #     """ Accessibility as I first understood it. It seems to give better
    #     results than the actual accessibility. """
    #     if walks is None:
    #         walks = self.__avoiding_walks__(node, length)
    #     nodes = [node for walk in walks for node in walk]
    #     total = len(nodes)
    #     proba = collections.Counter(nodes)
    #     return exp(-sum(p/total*log(p/total) for p in proba.values()))

    def __infected__(self, node):
        """ Builds the list of all possible orderings of the transmission
        events """
        res = set()
        for neigh1 in self.graph.neighbors(node):
            for neigh2 in self.graph.neighbors(node):
                if neigh1 != neigh2:
                    # print(neigh1, neigh2)
                    res.add((node, neigh1, neigh2))
        for (node, neigh1, neigh2) in self.__avoiding_walks__(node, 2):
            res.add((node, neigh1, neigh2))
        return res

    def __normalized_degree__(self, nodes):
        """ Computes the degree of a set of nodes (number of edges with one
        endpoint in the set) """
        res = set()
        for node in nodes:
            res = res.union(set(self.graph.neighbors(node)))
        return len(res.difference(set(nodes)))

    def expected_force(self, node):
        """ Computes the expected force of a node as explained in
        https://doi.org/10.1038%2Fsrep08665"""
        norm = sum(self.__normalized_degree__(nodes)
                   for nodes in self.__infected__(node))
        alpha = 2
        return log(alpha * self.graph.degree(node)) * (-sum(
            (self.__normalized_degree__(nodes)/norm) *
            log((self.__normalized_degree__(nodes)/norm), 10) for nodes in
            self.__infected__(node)))

    def expected_force_nolog(self, node):
        """ Computes the expected force of a node as explained in
        https://doi.org/10.1038%2Fsrep08665"""
        norm = sum(self.__normalized_degree__(nodes) for nodes in
                   self.__infected__(node))
        return (-sum(
            (self.__normalized_degree__(nodes)/norm) *
            log((self.__normalized_degree__(nodes)/norm)) for nodes in
            self.__infected__(node)))

    # def old_expected_force(self, node):
    #     """ Expected force as I first understood it. """
    #     return -sum(self.__normalized_degree__(nodes) *
    #                 log(self.__normalized_degree__(nodes)) for nodes in
    #                 self.__infected__(node))

    def __covered_trajectories_per_rectangle__(self, rectangles):
        """ Computes the amount of trajectories that cross each rectangle of
        the map, then computes the proportion of trajectories that are covered
        in a rectangle, by nodes of the rectangle.

        Takes rectangles, which is a list of stripes, which are lists of
        rectangles, which are lists of nodes.

        Then builds traj, which is the initial trajectories, stored in
        their respective rectangles. It is a list of stripes, which are lists
        of rectangles, which are lists of trajectories, which are lists of
        nodes, which are integers.

        Then generates a tuple (a, b, c, d) where:
            a is the number of trajectories uncovered in the rectangle,
            b is the number of trajectories uncovered or covered once,
            c is the number of trajectories covered less than 4 times and
            d is the total amount of trajectories going through that rectangle.

        Note that a trajectory only need one of its node in the rectangle to be
        considered part of the rectangle.

        Note also that a trajectory is considered covered if it has a node in
        common with the k_vertex_cover.  That node in common might not be in
        the same rectangle. """

        traj = list()
        for stripe in rectangles:
            traj.append(list())
            for square in stripe:
                traj[-1].append(set())
                for trajectory in self.trajectories:
                    if [node for node in trajectory
                            if node[0] in square or node[1] in square]:
                        traj[-1][-1].add(tuple(trajectory))

        traj_cov = list()
        # for i, stripe in enumerate(traj):
        for stripe in traj:
            traj_cov.append(list())
            # for j, rect in enumerate(stripe):
            for rect in stripe:
                count0 = 0
                count2 = 0
                count4 = 0
                for trajectory in rect:
                    # If covered means covered by a node of the rectangle:
                    # rec_vc = self.k_vertex_cover.intersection(
                    # rectangles[i][j])
                    # Or just use self.k_vertex_cover instead of rec_vc
                    covered = {node for node in trajectory
                               if node[0] in self.k_vertex_cover
                               or node[1] in self.k_vertex_cover}
                    if not covered:
                        count0 += 1
                    if len(covered) < 2:
                        count2 += 1
                    if len(covered) < 4:
                        count4 += 1
                traj_cov[-1].append((count0, count2, count4, len(rect)))
        return traj_cov

    def covered_trajectories_per_rectangle(self, n_rect, coords_file):
        """ Computes the proportion of trajectories covered, for rectangles of
        size n_rect """
        return self.__covered_trajectories_per_rectangle__(
            __square_to_set__(n_rect, coords_file))

    def __reconstruct_coordinates__(self, coords_file):
        """ Estimates coordinates of nodes for which we miss the coordinates
        but for which all neighbours' coordinates are known. """

        coords = __get_coords__(coords_file)
        reconstructed_new = dict()

        for node in self.graph.nodes():
            bad = 0
            coords_neighbours = dict()
            if node not in coords:
                for neighbour in self.graph.neighbors(node):
                    if neighbour in coords:
                        coords_neighbours[neighbour] = coords[neighbour]
                    else:
                        bad = 1

                if not bad:
                    lat, lon = 0, 0
                    for _, coord in coords_neighbours.items():
                        lat += float(coord[0])
                        lon += float(coord[1])
                    lat /= len(coords_neighbours)
                    lon /= len(coords_neighbours)
                    reconstructed_new[node] = (lat, lon)

        # coords_file = open("reconstructed_coordinates_no_traj2", 'wb')
        # pickle.dump(reconstructed_new, coords_file)
        # coords_file.close()
        return reconstructed_new

    def distances_trajectories(self, coords_file):
        """
        842 nodes missing in coordinates_cologne_30772 (all 66
        Added[On|Off]RampNodes, 55 out of 1455 clusters and 721 regular nodes).
        423 nodes aren't found.

        In any case, I will approximate the coordinates with:
        lat = lat1 + … + latn / n
        lon = lon1 + … + lonn / n

        Problem if a node and its neighbour aren't in Openstreetmap.
        """
        coords = __get_coords__(coords_file)
        trajectories_length = list()

        count = 0
        node1_bkp = 0
        for trajectory in self.trajectories:
            length = 0
            for node1, node2 in trajectory:
                if node2 not in coords and node1_bkp == 0:
                    node1_bkp = node1
                else:
                    if node1_bkp != 0 and node2 in coords:
                        node1 = node1_bkp
                        node1_bkp = 0
                    if node1 in coords and node2 in coords:
                        length += haversine_distance(coords[node1][0],
                                                     coords[node1][1],
                                                     coords[node2][0],
                                                     coords[node2][1])
            trajectories_length.append((trajectory, length))
            count += 1
            if count % 10000 == 0:
                logging.log(25, "%d %d", count, len(self.trajectories))

        return trajectories_length

    def centralities_most_traj(self):
        """ Generates a list of nodes, ordered by the amount of trajectories
        that cross them. Along with the centralities of each node. """
        sommets = collections.defaultdict(int)
        for trajectory in self.trajectories:
            for node1, _ in trajectory:
                sommets[node1] += 1
            sommets[trajectory[-1][1]] += 1
        for node1 in self.graph.nodes():
            if node1 not in sommets.keys():
                sommets[node1] = 0
        res = list()
        katz = self.__get_centrality__("katz")
        degree = self.__get_centrality__("degree")
        information = self.__get_centrality__("information")
        betweenness = self.__get_centrality__("betweenness")
        closeness = self.__get_centrality__("closeness")
        accessibility = self.__get_centrality__("accessibility")
        expected_force = self.__get_centrality__("expected_force")
        # expected_force_nolog = self.__get_centrality__("expected_force_nolog")
        eigenvector = self.__get_centrality__("eigenvector")
        # accessibility1 = self.__get_centrality__("accessibility", 1)
        # routes = open('accessibility_fixed_15_normalized_average', 'rb')
        # averageaccessibility = pickle.load(routes)
        # routes.close()
        for node in list(reversed(sorted(sommets, key=sommets.get))):
            res.append((node,
                        katz[node],
                        degree[node],
                        information[node],
                        betweenness[node],
                        closeness[node],
                        accessibility[node],
                        eigenvector[node],
                        expected_force[node],
                        sommets[node])
                       )
        # routes = open("nodes_ordered_traj_ixed_15_normalized_average", 'wb')
        # pickle.dump(res, routes)
        # routes.close()
        return res

    def __export_submap__(self, coords_file):
        """ Exports the graph and trajectories going through the rectangle of
        height and width 1/11th of the original rectangle, located at
        coordinates (4,4). """
        count = 0
        trajcv = list()
        logging.log(25, "Building map…")
        downtown = __square_to_set__(11, coords_file)[4][4]
        logging.log(25, "Building done")

        for node in set(self.graph.nodes()):
            if node not in downtown:
                self.graph.remove_node(node)

        for traj in self.trajectories:
            bad = 0
            for node1, node2 in traj:
                if node1 not in downtown or node2 not in downtown:
                    bad = 1
            if not bad:
                trajcv.append(traj)

            count += 1
            if count % 100000 == 0:
                logging.log(25, "%d out of %d", count, len(self.trajectories))
        self.trajectories = trajcv
        networkx.write_edgelist(self.graph, "cologne_downtown_fixed.graph")
        routes = open("cologne_downtown_fixed.routes", 'wb')
        pickle.dump(self.trajectories, routes)
        routes.close()

    def __add_weight__(self):
        """ Weights edges according to the amount of trajectories """
        max_weight = 0
        for trajectory in self.trajectories:
            for node_u, node_v in trajectory:
                if 'weight' not in self.graph[node_u][node_v]:
                    self.graph[node_u][node_v]['weight'] = 1
                else:
                    self.graph[node_u][node_v]['weight'] += 1
                    if self.graph[node_u][node_v]['weight'] > max_weight:
                        max_weight = self.graph[node_u][node_v]['weight']
        for node_u, node_v in self.graph.edges:
            if 'weight' not in self.graph[node_u][node_v]:
                # print("Removing", node_u, node_v, self.graph[node_u][node_v])
                self.graph.remove_edge(node_u, node_v)
                # self.graph[node_u][node_v] = 0
            else:
                self.graph[node_u][node_v]['weight'] /= max_weight

    def __generate_all_shortest_paths__(self):
        trajectories = list()
        sps = networkx.all_pairs_shortest_path(self.graph)
        count = 0
        for _, paths in sps:
            for nodes in paths.values():
                trajectory = list()
                for i in range(len(nodes[:-1])):
                    trajectory.append((nodes[i], nodes[i+1]))
                if trajectory:
                    trajectories.append(trajectory)
                count += 1
                if count % 10000 == 0:
                    logging.log(25, "%d out of %d", count,
                                len(self.graph.nodes) ** 2 -
                                len(self.graph.nodes))
        return trajectories

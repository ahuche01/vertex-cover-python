#!/usr/bin/python3

""" Rectangles functions """

import logging
import pickle
from math import radians, cos, sin, asin, sqrt
import overpy  # type: ignore


def __square_list__(n_rect, coords_file):
    """ Divides Cologne into n_rect² rectangles """
    coords = __get_coords__(coords_file)

    min_lat = min([elt[0] for elt in coords.values()])
    max_lat = max([elt[0] for elt in coords.values()])
    min_lon = min([elt[1] for elt in coords.values()])
    max_lon = max([elt[1] for elt in coords.values()])
    # Divides the area into stripes
    stripes = __divide__(coords, min_lat, max_lat, n_rect, 0)
    # Then divides the stripes into rectangles
    result = list()
    for stripe in stripes:
        result.append(__divide__(stripe, min_lon, max_lon, n_rect, 1))
    return result


def __divide__(coords, min_l, max_l, n_rect, lat):
    """ Split coords into n chunks of equal size """
    # Latitude is the first item of the tuple. Longitude is the second.
    rectangles = list()
    for i in range(n_rect):
        rectangle = dict()
        for key, node in coords.items():
            if min_l + (i * ((max_l - min_l) / n_rect)) < node[lat] \
                    <= min_l + ((i + 1) * ((max_l - min_l) / n_rect)):
                rectangle[key] = node
        rectangles.append(rectangle)
        if i == 0:
            for key, value in coords.items():
                if min_l == value[lat]:
                    rectangles[i][key] = value
    flattened_coords = [item for sublist in rectangles for item in sublist]
    unmatched = [coord for coord in coords if coord not in flattened_coords]
    if unmatched:
        logging.log(25, "WARNING: A node wasn't put in a square %s",
                    str(unmatched))
    return rectangles


def __square_to_set__(n_rect, coords_file):
    """ Converts a list of lists of lists (squares) into a list of list of sets
    """
    squares = __square_list__(n_rect, coords_file)
    squares_set = list()
    for stripe in squares:
        squares_set.append(list())
        for square in stripe:
            squares_set[-1].append(set(square))
    return squares_set


def get_gps(nodes):
    """ Gets the GPS coordinates of the cover.

    The main overpass API instance says "You can safely assume that you
    don't disturbe other users when you do less than 10,000 queries per
    day and download less than 5 GB data per day"
    https://wiki.openstreetmap.org/wiki/Overpass_API#Public_Overpass_API_instances

    We are querying about 3000 nodes for each query.

    Mapped nodes are of the form 325712162-AddedOffRampNode or
    329286806#2-AddedOnRampNode or cluster_1486026152_265913481_423521426.
    The API doesn't seem to work for mapped nodes because "the only allowed
    values are positive integers", yet when I truncate the ID to get the
    integer only, *Added* seem not to work, while cluster* seem to work.

    For cluster nodes, just pick one node of the cluster? 1455 of them.
    The nodes I checked are very close together, at the same intersection.

    For Added[On|Off]RampNode nodes, what to do? Discard them? They're all
    of degree 2. 66 of them. They're all on highways. For example,
    60697259-AddedOffRampNode is at 51.04375,6.95371.

    """
    mapping_file = open('mapping', 'rb')
    mapping = pickle.load(mapping_file)
    mapping_file.close()
    query = "node(id: "
    for vertex in nodes:
        if vertex in mapping.keys():
            if 'cluster' in mapping[vertex]:
                query += str(mapping[vertex].split('_')[1]) + ", "
            elif 'Added' in mapping[vertex]:
                # This shouldn't happen if prune_vertex_cover was called
                logging.log(25, "WARNING: Added node %s : %s", vertex,
                            mapping[vertex])
        else:
            query += str(vertex) + ", "
    query = query[:-2] + ");out;"
    api = overpy.Overpass()
    # Avoids empty request
    if query != "node(id);out;":
        return api.query(query)
    return None

    # This is to dump all coordinates of the road graph. Don't do it since
    # nodes have been changed on Openstreetmap's since the first time I
    # queryied it and fewer nodes will show up.
    # res = api.query(query)
    # print("Querying", count, "nodes. Got", len(res.nodes), "coordinates.")
    # mapping_reversed = dict()
    # for key, value in mapping.items():
    #     if "cluster" in value:
    #         value = str(value.split('_')[1])
    #     if "Added" not in value:
    #         mapping_reversed[int(value)] = key

    # coords_dict = dict()
    # for node in res.nodes:
    #     if node.id in mapping_reversed:
    #         coords_dict[mapping_reversed[node.id]] = (node.lat, node.lon)
    #     else:
    #         coords_dict[node.id] = (node.lat, node.lon)

    # # routes = open("coordinates_cologne3", 'wb')
    # # pickle.dump(coords_dict, routes)
    # # routes.close()

    # return res, mapping_reversed, mapping, coords_dict


def haversine_distance(lat1, lon1, lat2, lon2):
    """ Consistent with both another online calculator and openstreetmap + my
    good ruler """

    # Coordinate of the Jan von Werth Fountain, which I will assume is the
    # centre of Cologne: 50.9385561, 6.9600561.
    latitude_fountain = 50.9385561
    radius_equator = 6378.1370
    radius_pole = 6356.7523
    radius_cologne = sqrt(((radius_equator**2 * cos(latitude_fountain))**2 +
                           (radius_pole**2 * sin(latitude_fountain))**2) /
                          ((radius_equator * cos(latitude_fountain))**2 +
                           (radius_pole * sin(latitude_fountain))**2))

    lat_difference = radians(lat2 - lat1)
    lon_difference = radians(lon2 - lon1)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    return radius_cologne * 2 * asin(sqrt(sin(lat_difference/2)**2 +
                                          cos(lat1) * cos(lat2) *
                                          sin(lon_difference/2)**2))


def __get_coords__(coords_file):
    """ Reads from files where coordinates are stored.
        Format coords[node_id] = (lat, lon) """
    # coords_file = open("coordinates_cologne_30772", 'rb')
    # coords = pickle.load(coords_file)
    # coords_file.close()

    # coords_file = open("reconstructed_coordinates_traj", 'rb')
    # reconstructed_coords_traj = pickle.load(coords_file)
    # coords_file.close()

    # coords_file = open("reconstructed_coordinates_no_traj", 'rb')
    # reconstructed_coords_no_traj = pickle.load(coords_file)
    # coords_file.close()

    # coords.update(reconstructed_coords_traj)
    # coords.update(reconstructed_coords_no_traj)

    coords_file = open(coords_file, 'rb')
    coords = pickle.load(coords_file)
    coords_file.close()

    return coords
